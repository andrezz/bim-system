<?php

namespace Database\Seeders;

use App\Models\Area;
use Illuminate\Database\Seeder;

class AreaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Area::insert([
            [
                'serie_id' => 1,
                'name' => 'Software',
                'code' => 1,
                'description' => 'Aplicaciones, entregables y datos.'
            ], [
                'serie_id' => 1,
                'name' => 'Hardware',
                'code' => 2,
                'description' => 'Equipos, entregables y localización/movilidad.'
            ], [
                'serie_id' => 1,
                'name' => 'Red',
                'code' => 3,
                'description' => 'Soluciones, entregables y control de seguridad/ acceso.'
            ], [
                'serie_id' => 1,
                'name' => 'LOD y LOI',
                'code' => 4,
                'description' => ''
            ], [
                'serie_id' => 2,
                'name' => 'Recursos',
                'code' => 1,
                'description' => 'Infraestructura física y de conocimiento.'
            ], [
                'serie_id' => 2,
                'name' => 'Actividades & Flujos de trabajo',
                'code' => 2,
                'description' => 'Conocimiento, habilidades, experiencia, roles y dinámicas relevante.'
            ], [
                'serie_id' => 2,
                'name' => 'Liderazgo & Gestión',
                'code' => 3,
                'description' => 'Cualidades de organización, estratégicas, de gestión y comunicativas; innovación y renovación.'
            ], [
                'serie_id' => 3,
                'name' => 'Preparatorio',
                'code' => 1,
                'description' => 'Investigación, programas de educación/formación y entregables.'
            ], [
                'serie_id' => 3,
                'name' => 'Regulador',
                'code' => 2,
                'description' => 'Códigos, regulaciones, estándares, clasificaciones, directivas y referencias.'
            ], [
                'serie_id' => 4,
                'name' => 'Modelado basado en objetos',
                'code' => 1,
                'description' => 'Uso en una sola disciplina en una fase del ciclo de vida.'
            ], [
                'serie_id' => 4,
                'name' => 'Colaboración basada en el Modelo',
                'code' => 2,
                'description' => 'Multidisciplinar, intercambio por la vía rápida de modelos.'
            ], [
                'serie_id' => 4,
                'name' => 'Integración basada en la red',
                'code' => 3,
                'description' => 'Intercambio concurrente interdisciplinario de modelos de nD a lo largo de las Fases del Ciclo de Vida del Proyecto.'
            ], [
                'serie_id' => 4,
                'name' => 'Organizaciones',
                'code' => 4,
                'description' => 'Dinámicas y entregables BIM.'
            ], [
                'serie_id' => 4,
                'name' => 'Equipos de Proyecto',
                'code' => 5,
                'description' => '(múltiples organizaciones): dinámicas y entregables BIM inter-organizacional.'
            ]
        ]);
    }
}
