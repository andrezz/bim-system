<?php

namespace Database\Seeders;

use App\Models\Question;
use Illuminate\Database\Seeder;

class QuestionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Question::insert([
            [
                'area_id' => 1,
                'level_id' => 1,
                'code' => 1,
                'question' => 'Uso de aplicaciones de software no monitorizado ni regulado. Los Modelos 3D se usan como base para generar principalmente representaciones 2D / entregables precisos. El uso, almacenamiento e intercambio de datos no se definen dentro de las organizaciones o equipos de proyectos. Los intercambios sufren de una falta grave de interoperabilidad',
                'score' => 1
            ],
            [
                'area_id' => 1,
                'level_id' => 2,
                'code' => 2,
                'question' => 'El uso / introducción de Software se unifica dentro de una organización o equipos de proyecto (múltiples organizaciones). Los Modelos 3D se utilizan como base para generar tanto entregables 2D como 3D. El uso, almacenamiento e intercambio de datos están bien definidos dentro de las organizaciones y equipos de proyecto. Los intercambios de datos interoperables están definidos y priorizados',
                'score' => 2
            ],
            [
                'area_id' => 1,
                'level_id' => 3,
                'code' => 3,
                'question' => 'La selección de software y su uso se controla y gestiona de acuerdo con los entregables definidos. Los modelos son la base para las vistas 3D, representaciones 2D, cuantificación, especificación y estudios analíticos. El uso, almacenamiento e intercambio de datos son monitoreados y controlados. El flujo de datos está documentado y bien gestionado. Los intercambios de datos interoperables son obligatorios y se controlan con rigor',
                'score' => 3
            ],
            [
                'area_id' => 1,
                'level_id' => 4,
                'code' => 4,
                'question' => 'La selección e implementación de software sigue objetivos estratégicos, no sólo necesidades operacionales. Los entregables del modelado están bien sincronizados a través de proyectos y estrechamente integrados con los procesos de negocio. El uso, almacenamiento e intercambio de datos interoperables están regulados y se llevan a cabo como parte de una estrategia global de la organización o equipo de proyecto.',
                'score' => 4
            ],
            [
                'area_id' => 1,
                'level_id' => 5,
                'code' => 5,
                'question' => 'La selección / uso de herramientas de software se revisa continuamente para mejorar la productividad y se alinea con los objetivos estratégicos. Los entregables del modelado se revisan / optimizan cíclicamente para beneficiarse de las nuevas funcionalidades y extensiones disponibles de software. Todos los asuntos relacionados con el almacenamiento, uso e intercambio de datos interoperables están documentados, controlados, reflexionados y mejorados de
                forma proactiva.',
                'score' => 5
            ],
            [
                'area_id' => 2,
                'level_id' => 1,
                'code' => 1,
                'question' => 'Los equipos BIM son inadecuados; las especificaciones son demasiado bajas o inconsistentes en toda la organización. La sustitución o mejora de equipos se considera un coste y sólo se realiza cuando es inevitable.',
                'score' => 1
            ],
            [
                'area_id' => 2,
                'level_id' => 2,
                'code' => 2,
                'question' => 'Las especificaciones de los equipos - adecuados para la realización de productos y servicios BIM - se definen, presupuestan y estandarizan en toda la organización. Las sustituciones y actualizaciones de hardware son partidas de coste bien definidas.',
                'score' => 2
            ],
            [
                'area_id' => 2,
                'level_id' => 3,
                'code' => 3,
                'question' => 'Se dispone de una estrategia para documentar, gestionar y mantener los equipos BIM con transparencia. La inversión en hardware está bien orientada para mejorar la movilidad del personal (en caso necesario) y ampliar la productividad BIM.',
                'score' => 3
            ],
            [
                'area_id' => 2,
                'level_id' => 4,
                'code' => 4,
                'question' => 'Los despliegues de equipos se tratan como facilitadores BIM. La inversión en equipos se integra perfectamente con los planes financieros, estrategias de negocio y los objetivos de desempeño.',
                'score' => 4
            ],
            [
                'area_id' => 2,
                'level_id' => 5,
                'code' => 5,
                'question' => 'Los equipos existentes y las soluciones innovadoras se prueban, actualizan y despliegan continuamente. El hardware BIM se convierte en parte de la ventaja competitiva de la organización o del equipo de proyecto.',
                'score' => 5
            ],
            [
                'area_id' => 4,
                'level_id' => 1,
                'code' => 1,
                'question' => '',
                'score' => 1
            ],
            [
                'area_id' => 4,
                'level_id' => 2,
                'code' => 2,
                'question' => '',
                'score' => 2
            ],
            [
                'area_id' => 4,
                'level_id' => 3,
                'code' => 3,
                'question' => '',
                'score' => 3
            ],
            [
                'area_id' => 4,
                'level_id' => 4,
                'code' => 4,
                'question' => '',
                'score' => 4
            ],
            [
                'area_id' => 4,
                'level_id' => 5,
                'code' => 5,
                'question' => '',
                'score' => 5
            ],
            [
                'area_id' => 3,
                'level_id' => 1,
                'code' => 1,
                'question' => 'Las soluciones de red no existen o son ad-hoc. Profesionales, organizaciones (en un lugar/ disperso) y equipos de proyecto usan cualquier herramienta para comunicarse o compartir datos. Las partes interesadas carecen de la infraestructura de red necesaria para recopilar, almacenar y compartir conocimientos.',
                'score' => 1
            ],
            [
                'area_id' => 3,
                'level_id' => 2,
                'code' => 2,
                'question' => 'Se identifican soluciones de red para compartir información y controlar su acceso en y entre organizaciones. A nivel de proyecto, los agentes identifican sus requerimientos para compartir datos/información. Las organizaciones y equipos de proyecto dispersos están conectados a través de conexiones de ancho de banda relativamente bajo.',
                'score' => 2
            ],
            [
                'area_id' => 3,
                'level_id' => 3,
                'code' => 3,
                'question' => 'Las soluciones de red para recopilar, almacenar y compartir el conocimiento en y entre organizaciones se gestionan bien a través de plataformas comunes (por ejemplo: intranets o extranets). Se despliegan herramientas de gestión de contenidos y activos para regular los datos estructurados y no estructurados compartidos a través de conexiones de banda
                ancha.',
                'score' => 3
            ],
            [
                'area_id' => 3,
                'level_id' => 4,
                'code' => 4,
                'question' => 'Las soluciones de red permiten la integración de múltiples facetas del proceso BIM a través del intercambio en tiempo real continuo de datos, información y conocimientos. Las soluciones incluyen redes / portales específicos del proyecto que permiten el intercambio de datos intensivos (intercambio) interoperable entre las partes interesadas.',
                'score' => 4
            ],
            [
                'area_id' => 3,
                'level_id' => 5,
                'code' => 5,
                'question' => 'Las soluciones de red se evalúan continuamente y se sustituyen por las últimas innovaciones probadas. Las redes facilitan la adquisición, almacenar y compartir conocimientos entre todas las partes interesadas. La optimización de datos integrados, los procesos y los canales de comunicación es implacable.',
                'score' => 5
            ],
            [
                'area_id' => 5,
                'level_id' => 1,
                'code' => 1,
                'question' => 'El entorno de trabajo, o bien no se reconoce como un factor de la satisfacción del personal o puede no ser propicio para la productividad. El conocimiento no es reconocido como un activo; el conocimiento BIM suele compartirse de manera informal entre el personal (a través de consejos, técnicas y lecciones aprendidas).',
                'score' => 1
            ],
            [
                'area_id' => 5,
                'level_id' => 2,
                'code' => 2,
                'question' => 'El entorno de trabajo y las herramientas en el lugar de trabajo se identifican como factores que influyen en la motivación y la productividad. Del mismo modo, el conocimiento es reconocido como un activo; el conocimiento compartido es recopilado, documentado y después transferido de tácito a explícito.',
                'score' => 2
            ],
            [
                'area_id' => 5,
                'level_id' => 3,
                'code' => 3,
                'question' => 'El entorno de trabajo es controlado, modificado y sus criterios gestionados para aumentar la motivación del personal, la satisfacción y la productividad. Además, el conocimiento documentado se almacena adecuadamente.',
                'score' => 3
            ],
            [
                'area_id' => 5,
                'level_id' => 4,
                'code' => 4,
                'question' => 'Los factores ambientales se integren en las estrategias de desempeño. El conocimiento se integra en los sistemas de organización; el conocimiento almacenado se hace accesible y fácilmente recuperable.',
                'score' => 4
            ],
            [
                'area_id' => 5,
                'level_id' => 5,
                'code' => 5,
                'question' => 'Los factores físicos del lugar de trabajo se revisan constantemente para asegurar la satisfacción del personal y un entorno propicio para la productividad. Del mismo modo, las estructuras de conocimiento responsables de la adquisición, representación y difusión se revisan y modifican sistémicamente.',
                'score' => 5
            ],
            [
                'area_id' => 6,
                'level_id' => 1,
                'code' => 1,
                'question' => 'No hay procesos definidos; los roles son ambiguos y estructuras de equipo / dinámicas son inconsistentes. El rendimiento es impredecible y la productividad depende de heroicidades individuales. Florece una mentalidad de trabajo en torno al sistema.',
                'score' => 1
            ],
            [
                'area_id' => 6,
                'level_id' => 2,
                'code' => 2,
                'question' => 'Los roles BIM se definen informalmente y los equipos se forman en consecuencia. Cada proyecto BIM se planifica de forma independiente. Se identifican las competencias BIM y se objetivan; el heroísmo BIM se desvanece a medida que aumenta la competencia, pero la productividad sigue siendo impredecible.',
                'score' => 2
            ],
            [
                'area_id' => 6,
                'level_id' => 3,
                'code' => 3,
                'question' => 'La cooperación en las organizaciones aumenta a medida que se ponen a disposición las herramientas para la comunicación entre proyectos. Flujo de información constante; los roles BIM son visibles y los objetivos se consiguen de forma más consistente.',
                'score' => 3
            ],
            [
                'area_id' => 6,
                'level_id' => 4,
                'code' => 4,
                'question' => 'Los roles BIM y los objetivos de competencia se arraigan en la organización. Los equipos tradicionales son sustituidos por otros orientados a BIM a medida que los nuevos procesos se convierten en parte de la cultura de la organización / del equipo del proyecto. La productividad es ahora consistente y predecible.',
                'score' => 4
            ],
            [
                'area_id' => 6,
                'level_id' => 5,
                'code' => 5,
                'question' => 'Los objetivos de competencia BIM mejoran de manera continua para que coincidan con los avances tecnológicos y se alineen con los objetivos organizacionales. Las prácticas de recursos humanos se revisan de forma proactiva para asegurar que el capital intelectual coincida con las necesidades del proceso.',
                'score' => 5
            ],
            [
                'area_id' => 7,
                'level_id' => 1,
                'code' => 1,
                'question' => 'Los líderes / gerentes tienen varias visiones sobre BIM. La implementación de BIM (según los requisitos BIM de la etapa) se lleva a cabo sin una estrategia. En este nivel de madurez, BIM se trata como una corriente tecnológica; la innovación no se reconoce como un valor independiente y no se reconocen las oportunidades de negocios que surgen de BIM.',
                'score' => 1
            ],
            [
                'area_id' => 7,
                'level_id' => 2,
                'code' => 2,
                'question' => 'Los líderes / gerentes adoptan una visión común sobre BIM. La estrategia de implementación de BIM carece de datos procesables. BIM se trata como un proceso de cambio, una corriente tecnológica. Se reconocen las innovaciones de producto y proceso; Se identifican las oportunidades de negocio derivadas de BIM, pero no se explotan.',
                'score' => 2
            ],
            [
                'area_id' => 7,
                'level_id' => 3,
                'code' => 3,
                'question' => 'Se comunica la visión de implementar BIM y es entendida por la mayoría del personal. La estrategia de implementación BIM va de la mano con planes de acción detallados y un régimen de vigilancia. BIM es reconocido como una serie de tecnología, procesos y cambios en las políticas que deben ser gestionados sin poner trabas a la innovación. Se reconocen las oportunidades de negocio derivadas de BIM y se utilizan en las acciones de marketing.',
                'score' => 3
            ],
            [
                'area_id' => 7,
                'level_id' => 4,
                'code' => 4,
                'question' => 'La visión es compartida por el personal de toda la organización y / o los socios del proyecto. La implementación de BIM, sus requisitos y la innovación de procesos / productos están integrados en los canales organizativos, estratégicos, de gestión y de comunicación. Las oportunidades de negocio derivadas de BIM son parte de la ventaja competitiva del equipo,organización o del equipo de proyectos y se utilizan para atraer y mantener a los clientes.',
                'score' => 4
            ],
            [
                'area_id' => 7,
                'level_id' => 5,
                'code' => 5,
                'question' => 'Las partes interesadas han internalizado la visión BIM y se logra activamente. La estrategia de implementación de BIM y sus efectos en los modelos de organización se revisa de forma continua y alineada con otras estrategias. Si son necesarias modificaciones, se implementan de forma proactiva. El producto innovador / las soluciones de procesos y las oportunidades de negocio son codiciados y se persiguen de forma implacable.',
                'score' => 5
            ],
            [
                'area_id' => 8,
                'level_id' => 1,
                'code' => 1,
                'question' => 'Muy poca o ninguna formación a disposición del personal BIM. Los medios educativos / formativos no son adecuadas para alcanzar los resultados buscados.',
                'score' => 1
            ],
            [
                'area_id' => 8,
                'level_id' => 2,
                'code' => 2,
                'question' => 'Se definen los requisitos de formación y por lo general se proporcionan sólo cuando es necesario. Los medios de formación son diversos, permitiendo flexibilidad en la distribución de contenidos.',
                'score' => 2
            ],
            [
                'area_id' => 8,
                'level_id' => 3,
                'code' => 3,
                'question' => 'Los requisitos de formación se gestionan para cumplir con las competencias pre-establecidas y los objetivos de desempeño. Los medios de formación se adaptan a los alumnos y para alcanzar los objetivos de aprendizaje de una manera rentable.',
                'score' => 3
            ],
            [
                'area_id' => 8,
                'level_id' => 4,
                'code' => 4,
                'question' => 'La formación se integra en las estrategias de organización y objetivos de desempeño. La formación se basa típicamente en las funciones del personal y los objetivos de competencia respectivos. Los medios de formación se incorporan en los canales de conocimiento y
                comunicación.',
                'score' => 4
            ],
            [
                'area_id' => 8,
                'level_id' => 5,
                'code' => 5,
                'question' => 'La formación se evalúa y mejora de forma continua. La disponibilidad de formación y los métodos de entrega se diseñan para permitir el aprendizaje continuo multimodal.',
                'score' => 5
            ],
            [
                'area_id' => 9,
                'level_id' => 1,
                'code' => 1,
                'question' => 'No hay directrices BIM, protocolos de documentación o estándares de modelado. No hay estándares de documentación y modelado. Los planes de control de calidad son informales o no existen; tampoco para los modelos 3D o la documentación. No hay referencias para procesos, productos o servicios.',
                'score' => 1
            ],
            [
                'area_id' => 9,
                'level_id' => 2,
                'code' => 2,
                'question' => 'Existen unas directrices BIM disponibles (ex: manual de formación y estándares de ejecución BIM). Los estándares de Modelado y documentación están bien definidos, de acuerdo con los estándares aceptados del mercado. Se fijan los objetivos de calidad y las referencias de desempeño.',
                'score' => 2
            ],
            [
                'area_id' => 9,
                'level_id' => 3,
                'code' => 3,
                'question' => 'Hay unas directrices BIM detalladas disponibles (formación, estándares, flujos, excepciones...). El modelado, la representación, la cuantificación, las especificaciones y las propiedades analíticas de los modelos 3D se gestionan mediante estándares de modelado detallado y planes de calidad. Se monitoriza y controla estrechamente el desempeño frente a
                referencias del mercado.',
                'score' => 3
            ],
            [
                'area_id' => 9,
                'level_id' => 4,
                'code' => 4,
                'question' => 'Las directrices BIM están integradas en las políticas globales y las estrategias de negocio. Los estándares BIM y las referencias de desempeño se incorporan en los sistemas de gestión de calidad y de mejora de ejecución.',
                'score' => 4
            ],
            [
                'area_id' => 9,
                'level_id' => 5,
                'code' => 5,
                'question' => 'Las directrices BIM se redefinen continua y proactivamente para reflejar las lecciones aprendidas y las mejores prácticas de la industria. Se alinean continuamente la mejora de calidad y el cumplimiento de normativa y regulaciones. Las referencias se revisan de forma reiterada para asegurar la mayor calidad en procesos, productos y servicios.',
                'score' => 5
            ],
            [
                'area_id' => 10,
                'level_id' => 1,
                'code' => 1,
                'question' => 'Implementación de una herramienta basada en objetos. No se identifican cambios de proceso o en las políticas para acompañar esta
                implementación.',
                'score' => 1
            ],
            [
                'area_id' => 10,
                'level_id' => 2,
                'code' => 2,
                'question' => 'Se han acabado los proyectos piloto. Se identifican los requisitos del proceso y de la política BIM. Se prepara la estrategia de implementación y
                los planes de detalle.',
                'score' => 2
            ],
            [
                'area_id' => 10,
                'level_id' => 3,
                'code' => 3,
                'question' => 'Se instigan, estandarizan y controlan los procesos y la política BIM.',
                'score' => 3
            ],
            [
                'area_id' => 10,
                'level_id' => 4,
                'code' => 4,
                'question' => 'Las tecnologías, procesos y política BIM están integradas en las estrategias de organización y alineadas con los objetivos de negocio.',
                'score' => 4
            ],
            [
                'area_id' => 10,
                'level_id' => 5,
                'code' => 5,
                'question' => 'Las tecnologías, procesos y política BIM se revisan continuamente para beneficiarse de la innovación y alcanzar los objetivos de
                desempeño más altos.',
                'score' => 5
            ],
            [
                'area_id' => 11,
                'level_id' => 1,
                'code' => 1,
                'question' => 'Colaboración Ad-hoc BIM; las capacidades internas de colaboración son incompatibles con los socios del proyecto.
                Puede faltar confianza y respeto entre los participantes en el proyecto.',
                'score' => 1
            ],
            [
                'area_id' => 11,
                'level_id' => 2,
                'code' => 2,
                'question' => 'Colaboración BIM uno a uno, bien definida todavía reactiva. Hay señales identificables de la confianza mutua y el respeto entre los participantes del proyecto.',
                'score' => 2
            ],
            [
                'area_id' => 11,
                'level_id' => 3,
                'code' => 3,
                'question' => 'Colaboración proactiva entre las múltiples partes; los protocolos están bien documentados y gestionados. Existe confianza mutua, respeto y riesgos y beneficios compartidos entre los participantes del proyecto.',
                'score' => 3
            ],
            [
                'area_id' => 11,
                'level_id' => 4,
                'code' => 4,
                'question' => 'Colaboración entre las múltiples partes que incluye a los actores aguas abajo. Se caracteriza por la participación de los actores clave durante las fases iniciales del ciclo de vida del proyecto.',
                'score' => 4
            ],
            [
                'area_id' => 11,
                'level_id' => 5,
                'code' => 5,
                'question' => 'Equipo integrado por múltiples partes que incluye a todos los actores clave en un entorno caracterizado por la buena voluntad, la confianza y el respeto.',
                'score' => 5
            ],
            [
                'area_id' => 12,
                'level_id' => 1,
                'code' => 1,
                'question' => 'Los modelos integrados son generados por una serie limitada de participantes en el proyecto - posiblemente bajo barreras corporativas. La integración se produce con guías de procesos, normas o protocolos de intercambio poco o no pre- definidas. No hay una propuesta formal de las funciones y responsabilidades de los
                participantes.',
                'score' => 1
            ],
            [
                'area_id' => 12,
                'level_id' => 2,
                'code' => 2,
                'question' => 'Los modelos integrados son generados por un gran subconjunto de los participantes en el proyecto. La integración sigue guías de proceso, normas y protocolos de intercambio pre- definidas. Se distribuyen las responsabilidades y los riesgos se mitigan a través de medios contractuales.',
                'score' => 2
            ],
            [
                'area_id' => 12,
                'level_id' => 3,
                'code' => 3,
                'question' => 'Los modelos integrados (o partes de) son generados y gestionados por la mayoría de los participantes en el proyecto. Las responsabilidades dentro de alianzas temporales de proyecto o asociaciones a más largo plazo son claras. Los riesgos y beneficios se gestionan y distribuyen de forma activa.',
                'score' => 3
            ],
            [
                'area_id' => 12,
                'level_id' => 4,
                'code' => 4,
                'question' => 'Los modelos integrados son generados y gestionados por todos los participantes clave del proyecto. La integración basada en la red es la norma y el foco no está en la forma de integrar modelos / flujos de trabajo, sino en la detección y resolución proactiva de los desajustes de tecnología, procesos y políticas.',
                'score' => 4
            ],
            [
                'area_id' => 12,
                'level_id' => 5,
                'code' => 5,
                'question' => 'Se revisa y optimiza continuamente la integración de modelos y flujos de trabajo. Un equipo de proyecto interdisciplinar, estrechamente unido, persigue de forma activa nuevas eficiencias, entregables y alineaciones. Los modelos integrados son resultado de la aportación de muchos participantes en la cadena de
                suministro de la construcción.',
                'score' => 5
            ],
            [
                'area_id' => 13,
                'level_id' => 1,
                'code' => 1,
                'question' => 'No existe un liderazgo BIM; la implementación depende de los campeones de la tecnología.',
                'score' => 1
            ],
            [
                'area_id' => 13,
                'level_id' => 2,
                'code' => 2,
                'question' => 'Se formaliza el liderazgo BIM; los diferentes roles en el proceso de proceso de implementación están definidos.',
                'score' => 2
            ],
            [
                'area_id' => 13,
                'level_id' => 3,
                'code' => 3,
                'question' => 'Los roles BIM Pre-definidos se complementan entre ellos en la gestión del proceso de implementación.',
                'score' => 3
            ],
            [
                'area_id' => 13,
                'level_id' => 4,
                'code' => 4,
                'question' => 'Los roles BIM están integrados en las estructuras de liderazgo de la organización.',
                'score' => 4
            ],
            [
                'area_id' => 13,
                'level_id' => 5,
                'code' => 5,
                'question' => 'El liderazgo BIM muta continuamente para permitir nuevas tecnologías, procesos y entregables',
                'score' => 5
            ],
            [
                'area_id' => 14,
                'level_id' => 1,
                'code' => 1,
                'question' => 'Cada proyecto se ejecuta de forma independiente. No existe ningún acuerdo entre los agentes que intervienen para colaborar más allá del proyecto común
                actual',
                'score' => 1
            ],
            [
                'area_id' => 14,
                'level_id' => 2,
                'code' => 2,
                'question' => 'Los participantes piensan más allá de un solo proyecto. Se definen y documentan los protocolos de colaboración entre participantes del proyecto.',
                'score' => 2
            ],
            [
                'area_id' => 14,
                'level_id' => 3,
                'code' => 3,
                'question' => 'La colaboración entre múltiples organizaciones en varios proyectos se gestiona a través de alianzas temporales entre participantes.',
                'score' => 3
            ],
            [
                'area_id' => 14,
                'level_id' => 4,
                'code' => 4,
                'question' => 'Los proyectos de colaboración los realizan organizaciones interdisciplinares o equipos de proyectos multidisciplinares; una alianza entre muchos actores
                clave.',
                'score' => 4
            ],
            [
                'area_id' => 14,
                'level_id' => 5,
                'code' => 5,
                'question' => 'Los proyectos de colaboración son realizados por equipos de proyectos interdisciplinares auto - optimizados, que incluyen a la mayoría de los participantes',
                'score' => 5
            ],


        ]);
    }
}
