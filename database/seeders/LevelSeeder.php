<?php

namespace Database\Seeders;

use App\Models\Level;
use Illuminate\Database\Seeder;

class LevelSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Level::insert([
            [
                'code' => 1,
                'name' => 'INICIAL',
                'score' => 1
            ],[
                'code' => 2,
                'name' => 'DEFINIDO',
                'score' => 2
            ],[
                'code' => 3,
                'name' => 'GESTIONADO',
                'score' => 3
            ],[
                'code' => 4,
                'name' => 'INTEGRADO',
                'score' => 4
            ],[
                'code' => 5,
                'name' => 'OPTIMIZADO',
                'score' => 5
            ],
        ]);
    }
}
