<?php

namespace Database\Seeders;

use App\Models\Serie;
use Illuminate\Database\Seeder;

class SerieSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Serie::insert([
            [
                'survey_id' => 1,
                'name' => 'Tecnología',
                'code' => 1
            ],
            [
                'survey_id' => 1,
                'name' => 'Procesos',
                'code' => 2
            ],[
                'survey_id' => 1,
                'name' => 'Política',
                'code' => 3
            ],[
                'survey_id' => 1,
                'name' => 'Escala ORG',
                'code' => 4
            ],
        ]);
    }
}
