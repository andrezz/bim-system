<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    public function loginView()
    {
        if(Auth::check()){
            return redirect()->route('admin.users.dashboard');
        }
        return view('admin.users.login');
    }

    public function dashboard()
    {
        return view('admin.users.dashboard');
    }

    public function doLogin(Request $request)
    {
        $validated = $request->validate([
            'email' => 'required|email',
            'password' => 'required|alphaNum'
        ]);

        $data = [
            'email' => $request->get('email'),
            'password' => $request->get('password'),
        ];

        if (Auth::attempt($data)) {
            return redirect()->route('admin.users.dashboard');
        }
        return redirect()->back()
            ->withErrors(['auth' => 'Usuario o contraseña inválidos'])
            ->withInput();
    }
}
