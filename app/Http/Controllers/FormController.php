<?php

namespace App\Http\Controllers;

use App\Models\Application;
use App\Models\Area;
use App\Models\Form;
use App\Models\Level;
use App\Models\Organization;
use App\Models\Question;
use App\Models\Serie;
use App\Models\Survey;
use App\Models\Surveyed;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class FormController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $items = Organization::where('user_id', Auth::user()->id)->with('forms')->get();
        return view('admin.forms.index', [
            'organizations' => $items
        ]);
    }

    public function share(Request $request, $id)
    {
        return view('admin.forms.share', [
            'item' => Form::find($id)
        ]);
    }

    public function notFound()
    {
        return view('admin.forms.not-found');
    }

    public function applySurveyFromCode(Request $request)
    {
        return redirect()->route('forms.apply', ['code' => $request->get('code')]);
    }

    public function registerSurveyed(Request $request, $code)
    {
        $form = Form::where('code', $code)->with('organization')->first();

        if (!$form) {
            return redirect()->route('forms.not-found');
        }

        return view('survey.register', [
            'organization' => $form->organization,
            'code' => $code
        ]);
    }

    public function applySurvey(Request $request, $code)
    {
        if (!$request->session()->has('surveyedId') || !Surveyed::find(session('surveyedId'))) {
            $request->session()->forget('surveyedId');
            return redirect()->route('forms.register', ['code' => $code]);
        }

        $form = Form::where('code', $code)->first();

        if (!$form) {
            return redirect()->route('forms.not-found');
        }

        $application = (new Application())->getOrCreate($request->session()->get('surveyedId'), $form->id);

        if ($application->status == 1) {
            return redirect()->route('forms.result', ['code' => $code]);
        }

        $content = (new Application())->getFormContent($application->id);
        if ($content != null) {
            $content['code'] = $code;
            return view('survey.form', $content);
        }

        return redirect()->route('forms.result', ['code' => $code]);
    }

    public function showResult(Request $request, $code)
    {
        $form = Form::where('code', $code)->first();

        if (!$form) {
            return redirect()->route('forms.not-found');
        }
        $application = Application::where('surveyed_id', session('surveyedId'))
            ->where('form_id', $form->id)
            ->orderBy('created_at', 'DESC')
            ->first();

        if (!$application) {
            $request->session()->forget('surveyedId');
            return redirect()->route('forms.register', ['code' => $code]);
        }

        $result = (new Application())->getResult($application->id);

        if ($result['value'] == 0.0) {
            return redirect()->route('forms.apply', ['code' => $code]);
        }

        $result['id'] = $application->id;

        if ($application->status == 0) {
            $application->status = 1;
            $application->save();
        }

        $request->session()->forget('surveyedId');
        return view('survey.result', $result);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    public function storeAuto(Request $request, $organizationId)
    {
        $survey = Survey::first();
        $item = (new Form())->generateForm($organizationId, $survey->id);
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Form  $form
     * @return \Illuminate\Http\Response
     */
    public function show(Form $form)
    {

        return view('admin.forms.result', [
            'form' => $form,
            'series' => Serie::where('survey_id', $form->survey_id)->get(),
            'levels' => Level::all(),
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Form  $form
     * @return \Illuminate\Http\Response
     */
    public function edit(Form $form)
    {
        $maxRoundApplication = $form->applications()->max('round_number');
        return view('admin.forms.edit', [
            'form' => $form,
            'maxRoundApplication' => $maxRoundApplication || 1
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Form  $form
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Form $form)
    {
        $form->description = $request->get('description');
        $form->rounds = $request->get('rounds');
        $form->save();
        return redirect()->route('form.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Form  $form
     * @return \Illuminate\Http\Response
     */
    public function destroy(Form $form)
    {
        $form->delete();
        return redirect()->route('form.index');
    }
}
