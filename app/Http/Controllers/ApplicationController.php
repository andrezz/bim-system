<?php

namespace App\Http\Controllers;

use App\Models\Application;
use App\Models\Level;
use App\Models\Serie;
use Illuminate\Http\Request;
use Barryvdh\DomPDF\Facade\Pdf;

class ApplicationController extends Controller
{


    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Application  $application
     * @return \Illuminate\Http\Response
     */
    public function show(Application $application)
    {
        $pdf = Pdf::loadView('survey.result-pdf', [
            'application' => $application,
            'series' => Serie::where('survey_id', $application->form->survey_id)->get(),
            'levels' => Level::all()
        ]);
        return $pdf->stream('EncuestaBIM-'.$application->id.$application->created_at->format('dmYHi').'.pdf');
    }


}
