<?php

namespace App\Http\Controllers;

use App\Models\Surveyed;
use Illuminate\Http\Request;

class SurveyedController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.surveyeds.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.surveyeds.form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $item = Surveyed::where('code', $request->get('code'))->first();

        if ($item == null) {
            $item = new Surveyed();
        }

        $item->organization_id = $request->get('organization_id');
        $item->name = $request->get('name');
        $item->code = $request->get('code');
        $item->save();

        session(['surveyedId' => $item->id]);

        return redirect()->route('forms.apply', ['code' => $request->get('form_code')]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Surveyed  $surveyed
     * @return \Illuminate\Http\Response
     */
    public function show(Surveyed $surveyed)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Surveyed  $surveyed
     * @return \Illuminate\Http\Response
     */
    public function edit(Surveyed $surveyed)
    {
        return view('admin.surveyeds.form');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Surveyed  $surveyed
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Surveyed $surveyed)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Surveyed  $surveyed
     * @return \Illuminate\Http\Response
     */
    public function destroy(Surveyed $surveyed)
    {
        //
    }
}
