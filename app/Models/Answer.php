<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Answer extends Model
{
    use HasFactory;

    public function area()
    {
        return $this->belongsTo(Area::class);
    }

    public function question()
    {
        return $this->belongsTo(Question::class);
    }

    public function application()
    {
        return $this->belongsTo(Application::class);
    }
}
