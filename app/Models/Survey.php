<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Survey extends Model
{
    use HasFactory;

    public function series()
    {
        return $this->hasMany(Serie::class);
    }

    public function forms()
    {
        return $this->hasMany(Form::class);
    }

    public function getAreas($surveyId)
    {
        return Area::whereHas('serie', function ($query) use ($surveyId) {
            $query->where('survey_id', $surveyId);
        })
            ->with(['serie' => function ($query) {
                $query->orderBy('code', 'ASC');
            }])
            ->get();
    }
}
