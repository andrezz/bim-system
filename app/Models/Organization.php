<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Organization extends Model
{
    use HasFactory;

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function forms()
    {
        return $this->hasMany(Form::class);
    }

    public function surveyeds()
    {
        return $this->hasMany(Surveyed::class);
    }

    public function checkIfExists($code): bool
    {
        return $this->where('code', $code)->first() != null;
    }

    public function getTypes() {
        return [
            'PRIVATE' => 'Privada',
            'PUBLIC' => 'Pública'
        ];
    }
}
