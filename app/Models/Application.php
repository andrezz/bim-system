<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Application extends Model
{
    use HasFactory;

    public function surveyed()
    {
        return $this->belongsTo(Surveyed::class);
    }

    public function form()
    {
        return $this->belongsTo(Form::class);
    }

    public function answers()
    {
        return $this->hasMany(Answer::class);
    }

    public function getOrCreate($surveyedId, $formId)
    {
        $form = Form::find($formId);
        $item = Application::where('surveyed_id', $surveyedId)
            ->where('form_id', $formId)
            ->orderBy('created_at', 'DESC')
            ->first();

        $roundNumber = 1;

        if ($item != null && $item->status == 0) {
            return $item;
        }

        if ($item != null && $item->status == 1) {
            $roundNumber = $item->round_number + 1;
        }

        if ($roundNumber > $form->rounds && $item != null) {
            return $item;
        }

        $item = new Application();
        $item->surveyed_id = $surveyedId;
        $item->form_id = $formId;
        $item->round_number = $roundNumber;
        $item->score = 0.0;
        $item->status = 0;
        $item->save();
        return $item;
    }

    public function getFormContent($applicationId)
    {
        $item = Application::find($applicationId);
        $areas = (new Survey())->getAreas($item->form->survey_id);
        $answers = $item->answers;
        $nextArea = $this->getNextArea($answers, $areas);
        if ($nextArea) {
            $progress = $this->getProgress($answers, $areas) / count($areas) * 100;
            return [
                'area' => $nextArea,
                'application' => $item,
                'questions' => $nextArea->questions,
                'progress' => $progress
            ];
        }
        return null;
    }

    public function getResult($applicationId)
    {
        $item = Application::find($applicationId);
        $resultValue = Answer::where('application_id', $item->id)->sum('score');
        $areasCount = Area::whereHas('serie', function ($query) use ($item) {
            $query->where('survey_id', $item->form->survey_id);
        })->count();
        if ($areasCount > 0) {
            $resultValue = $resultValue / $areasCount;
        }
        $levels = Level::orderBy('code', 'ASC')->get();
        foreach ($levels as $level) {
            if ($level->score == intval(round($resultValue))) {
                return ['label' => $level->name, 'value' => $resultValue];
            }
        }
        return ['label' => 'SIN DEFINIR', 'value' => $resultValue];
    }

    private function getNextArea($answers, $areas)
    {
        foreach ($areas as $area) {
            $exists = false;
            foreach ($answers as $answer) {
                if ($answer->area_id == $area->id) {
                    $exists = true;
                }
            }
            if ($exists == false) {
                return $area;
            }
        }
        return null;
    }

    private function getProgress($answers, $areas)
    {
        foreach ($areas as $key => $area) {
            $exists = false;
            foreach ($answers as $answer) {
                if ($answer->area_id == $area->id) {
                    $exists = true;
                }
            }
            if ($exists == false) {
                return $key;
            }
        }
    }
}
