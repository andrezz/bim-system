<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Form extends Model
{
    use HasFactory;

    public function organization()
    {
        return $this->belongsTo(Organization::class);
    }

    public function survey()
    {
        return $this->belongsTo(Survey::class);
    }

    public function applications()
    {
        return $this->hasMany(Application::class);
    }

    public function generateCode(): string
    {
        return Str::random(4) . '-' . Str::random(4) . '-' . Str::random(4);
    }

    public function getDefaultRounds(): int
    {
        return 3;
    }

    public function generateForm($organizationId, $surveyId)
    {
        $item = new Form();
        $item->code = $this->generateCode();
        $item->description = 'Encuesta ' . Survey::find($surveyId)->name;
        $item->rounds = $this->getDefaultRounds();
        $item->organization_id = $organizationId;
        $item->survey_id = $surveyId;
        $item->save();
        return $item;
    }

    public function findByCode($code)
    {
        return $this->where('code', $code)->first();
    }

    public function getScore($formId)
    {
    }
}
