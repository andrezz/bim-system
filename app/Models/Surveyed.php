<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Surveyed extends Model
{
    use HasFactory;

    public function applications()
    {
        return $this->hasMany(Application::class);
    }

    public function organization()
    {
        return $this->belongsTo(Organization::class);
    }
}
