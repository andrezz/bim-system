<?php

use App\Http\Controllers\AnswerController;
use App\Http\Controllers\ApplicationController;
use App\Http\Controllers\AreaController;
use App\Http\Controllers\FormController;
use App\Http\Controllers\OrganizationController;
use App\Http\Controllers\QuestionController;
use App\Http\Controllers\SubAreaController;
use App\Http\Controllers\SurveyController;
use App\Http\Controllers\SurveyedController;
use App\Http\Controllers\UserController;
use App\Models\Application;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [SurveyController::class, 'welcomeView'])->name('survey.welcome');

Route::prefix('survey')->group(function () {
    Route::get('/register', [SurveyController::class, 'surveyRegisterView'])->name('survey.register');
    Route::get('/result', [SurveyController::class, 'surveyResultView'])->name('survey.result');
    Route::get('/result-pdf/{application}', [ApplicationController::class, 'show'])->name('survey.resultpdf');
});

Route::get('/form/not-found', [FormController::class, 'notFound'])->name('forms.not-found');
Route::get('/form/apply/{code}', [FormController::class, 'applySurvey'])->name('forms.apply');
Route::get('/form/apply-from-code', [FormController::class, 'applySurveyFromCode'])->name('forms.applyCode');
Route::get('/form/register/{code}', [FormController::class, 'registerSurveyed'])->name('forms.register');
Route::get('/form/result/{code}', [FormController::class, 'showResult'])->name('forms.result');

Route::prefix('admin')->group(function () {
    Route::get('/login', [UserController::class, 'loginView'])->name('admin.users.login');
    Route::post('/login', [UserController::class, 'doLogin'])->name('admin.users.doLogin');
    Route::get('/dashboard', [UserController::class, 'dashboard'])->name('admin.users.dashboard');
    Route::get('/form/auto/{organizationId}', [FormController::class, 'storeAuto'])->name('admin.forms.storeAuto');
    Route::get('/form/share/{id}', [FormController::class, 'share'])->name('admin.forms.share');

    Route::resource('survey', SurveyController::class);
    Route::resource('surveyed', SurveyedController::class);
    Route::resource('answer', AnswerController::class);
    Route::resource('area', AreaController::class);
    Route::resource('question', QuestionController::class);
    Route::resource('sub-area', SubAreaController::class);
    Route::resource('user', UserController::class);
    Route::resource('organization', OrganizationController::class);
    Route::resource('form', FormController::class);
});
