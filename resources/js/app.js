require('./bootstrap');

window.confirmDelete = (formId) => {
    const result = confirm('¿Está seguro(a) de eliminar este registro?');
    if(result){
        document.getElementById(formId).submit();
    }
}
