@props(['title'])
<div class="flex items-center justify-start border-slate-300 border-b py-3 px-4 mb-4 dark:border-slate-800 dark:bg-slate-800">
    <img src="{{asset('images/logo.svg')}}" alt="logo">
    <h1 class="text-lg font-bold mx-2 dark:text-white">{{$title}}</h1>
</div>
