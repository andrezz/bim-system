@extends('layout.admin')
@section('content')
    <div class="container mx-auto p-4">
        <div class="grid grid-cols-1 md:grid-cols-2">
            <a class="flex items-center text-gray-900 bg-white border border-gray-300 focus:outline-none hover:bg-gray-100 focus:ring-4 focus:ring-gray-200 font-medium rounded-lg text-sm px-5 py-5 mr-2 mb-2 dark:bg-gray-800 dark:text-white dark:border-gray-600 dark:hover:bg-gray-700 dark:hover:border-gray-600 dark:focus:ring-gray-700"
                href="{{ route('organization.index') }}">
                <span class="text-indigo-500 text-2xl mr-2">
                    <x-office />
                </span>
                Organizaciones
            </a>
            <a class="flex items-center text-gray-900 bg-white border border-gray-300 focus:outline-none hover:bg-gray-100 focus:ring-4 focus:ring-gray-200 font-medium rounded-lg text-sm px-5 py-5 mr-2 mb-2 dark:bg-gray-800 dark:text-white dark:border-gray-600 dark:hover:bg-gray-700 dark:hover:border-gray-600 dark:focus:ring-gray-700"
                href="{{ route('form.index') }}">
                <span class="text-indigo-500 text-2xl mr-2">
                    <x-folder />
                </span>
                Encuestas
            </a>
        </div>

    </div>
@endsection
