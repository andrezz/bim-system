@extends('layout.admin')
@section('content')
    <div class="container mx-auto px-4">
        <nav class="flex py-4 mb-4" aria-label="Breadcrumb">
            <ol class="inline-flex items-center space-x-1 md:space-x-3">
                <li class="inline-flex items-center">
                    <a href="{{ route('admin.users.dashboard') }}"
                        class="inline-flex items-center text-sm font-medium text-gray-700 hover:text-gray-900 dark:text-gray-400 dark:hover:text-white">
                        <x-home />
                        Inicio
                    </a>
                </li>
                <li aria-current="page">
                    <div class="flex items-center">
                        <x-chevron-right />
                        <span class="ml-1 text-sm font-medium text-gray-500 md:ml-2 dark:text-gray-400">
                            Compartir Encuesta
                        </span>
                    </div>
                </li>
            </ol>
        </nav>

        <div class="mb-4">
            <h1 class="font-bold text-lg mb-4 dark:text-white">Compartir Enlace por</h1>
            <a href="https://wa.me/?text=Hola%2C%20te%20env%C3%ADo%20el%20enlace%20para%20rellenar%20la%20encuesta%20BIM%0A{{route('forms.apply',['code' => $item->code])}}" target="_blank"
                class="text-gray-900 bg-white hover:bg-gray-100 border border-gray-200 focus:ring-4 focus:outline-none focus:ring-gray-100 font-medium rounded-lg text-sm px-5 py-2.5 text-center inline-flex items-center dark:focus:ring-gray-600 dark:bg-gray-800 dark:border-gray-700 dark:text-white dark:hover:bg-gray-700 mr-2 mb-2">
                <span class="text-green-500 mr-2">
                    <x-whatsapp />
                </span>
                WhatsApp
            </a>
            <a href="mailto:?body=Hola%2C%20te%20env%C3%ADo%20el%20enlace%20para%20rellenar%20la%20encuesta%20BIM%0A{{route('forms.apply',['code' => $item->code])}}"
                class="text-gray-900 bg-white hover:bg-gray-100 border border-gray-200 focus:ring-4 focus:outline-none focus:ring-gray-100 font-medium rounded-lg text-sm px-5 py-2.5 text-center inline-flex items-center dark:focus:ring-gray-600 dark:bg-gray-800 dark:border-gray-700 dark:text-white dark:hover:bg-gray-700 mr-2 mb-2">
                <span class="text-red-500 mr-2">
                    <x-email />
                </span>
                Correo
            </a>
        </div>

        <div class="mb-4">
            <h1 class="font-bold text-lg mb-4 dark:text-white">Copiar Código</h1>
            <div class="flex items-center justify-between gap-4">
                <input type="text" value="{{ $item->code }}"
                    class="text-copy-code grow bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"readonly>
                <button onclick="copyToClipboard('{{$item->code}}')"
                    class="btn-copy-code flex-none whitespace-nowrap text-white bg-indigo-700 hover:bg-indigo-800 focus:ring-4 focus:ring-indigo-300 font-medium rounded-lg text-sm px-5 py-2.5 dark:bg-indigo-600 dark:hover:bg-indigo-700 focus:outline-none dark:focus:ring-indigo-80">
                    <x-clipboard />
                </button>
            </div>
        </div>

        <div id="toast"
            class="p-4 mb-4 text-sm text-green-700 bg-green-100 rounded-lg dark:bg-green-200 dark:text-green-800"
            role="alert">
            <span class="font-medium">Código Copiado</span> Puedes compartir el código de la encuesta para que sea
            contestada.
        </div>
    </div>
    <script>
        function copyToClipboard(str) {
            const el = document.createElement('textarea');
            el.value = str;
            el.setAttribute('readonly', '');
            el.style.position = 'absolute';
            el.style.left = '-9999px';
            document.body.appendChild(el);
            el.select();
            document.execCommand('copy');
            document.body.removeChild(el);
            document.getElementById('toast').classList.remove('hidden');
            hideToast();
        }

        function hideToast(){
            setTimeout(function(){
                document.getElementById('toast').classList.add('hidden');
            }, 2000);
        }
        copyToClipboard("{{ $item->code }}");
        hideToast();
    </script>
@endsection
