@extends('layout.survey')
@section('content')
    <div class="container mx-auto p-6">
        <h1 class="text-center text-2xl font-bold">Encuesta no encontrada</h1>
        <p class="text-center text-slate-500">
            Es probable que la encuesta haya sido eliminada o ya no está disponible
            <br>
            <br>
            <a href="{{ url('/') }}"
            class="text-center mx-auto text-white bg-indigo-700 hover:bg-indigo-800 focus:ring-4 focus:ring-indigo-300 font-medium rounded-lg text-sm px-5 py-2.5 mr-2 mb-2 dark:bg-indigo-600 dark:hover:bg-indigo-700 focus:outline-none dark:focus:ring-indigo-800">Ingresar otro Código</a>
        </p>

    </div>
@endsection
