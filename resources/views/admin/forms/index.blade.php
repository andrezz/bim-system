@extends('layout.admin')
@section('content')
    <div class="container mx-auto px-4">
        <nav class="flex py-4 mb-4" aria-label="Breadcrumb">
            <ol class="inline-flex items-center space-x-1 md:space-x-3">
                <li class="inline-flex items-center">
                    <a href="{{ route('admin.users.dashboard') }}"
                        class="inline-flex items-center text-sm font-medium text-gray-700 hover:text-gray-900 dark:text-gray-400 dark:hover:text-white">
                        <x-home />
                        Inicio
                    </a>
                </li>
                <li aria-current="page">
                    <div class="flex items-center">
                        <x-chevron-right />
                        <span class="ml-1 text-sm font-medium text-gray-500 md:ml-2 dark:text-gray-400">
                            Encuestas
                        </span>
                    </div>
                </li>
            </ol>
        </nav>

        @foreach ($organizations as $org)
            <div class="p-4 mb-4">
                <div class="flex items-center justify-between mb-4">
                    <h1 class="text-medium flex-1 font-bold dark:text-white">{{ $org->name }}</h1>
                    <a href="{{ route('admin.forms.storeAuto', ['organizationId' => $org->id]) }}"
                        class="text-white bg-indigo-700 hover:bg-indigo-800 focus:ring-4 focus:outline-none focus:ring-indigo-300 font-medium rounded-lg text-sm px-4 py-2 text-center dark:bg-indigo-600 dark:hover:bg-indigo-700 dark:focus:ring-indigo-800">
                        Nueva Encuesta
                    </a>
                </div>
                @foreach ($org->forms as $item)
                    <div class="border-slate-300 dark:border-slate-800 dark:bg-slate-800 border rounded p-3 mb-4">
                        <div class="flex justify-between">
                            <div class="flex-1">
                                <h2 class="text-medium font-bold dark:text-white">{{ $item->description }}</h2>
                                <p class="text-sm font-bold text-slate-500">{{$item->code}}</p>
                                <span class="text-sm font-bold text-blue-500 dark:text-blue-300">{{ $item->rounds }}
                                    Ronda(s)</span>
                            </div>
                            <div class="flex">
                                <a href="{{ route('form.edit', ['form' => $item->id]) }}"
                                    class="text-gray-800 bg-white hover:bg-gray-100 border border-gray-200 focus:ring-4 focus:outline-none focus:ring-gray-100 font-medium rounded-lg text-sm px-2 py-1 text-center inline-flex items-center dark:focus:ring-gray-600 dark:bg-gray-800 dark:border-gray-700 dark:text-white dark:hover:bg-gray-700 mr-2 mb-2">
                                    <x-pencil-square />
                                </a>

                                <form id="form_delete_{{ $item->id }}"
                                    action="{{ route('form.destroy', ['form' => $item->id]) }}" method="POST">
                                    @csrf
                                    @method('DELETE')
                                </form>

                                <button onclick="confirmDelete('form_delete_{{ $item->id }}')"
                                    class="btn-delete text-pink-500 bg-white hover:bg-gray-100 border border-gray-200 focus:ring-4 focus:outline-none focus:ring-gray-100 font-medium rounded-lg text-sm px-2 py-1 text-center inline-flex items-center dark:focus:ring-gray-600 dark:bg-gray-800 dark:border-gray-700 dark:text-white dark:hover:bg-gray-700 mr-2 mb-2">
                                    <x-trash />
                                </button>
                            </div>
                        </div>
                        <p class="text-sm text-slate-800 mt-2 mb-4">Registrada el
                            {{ $item->created_at->format('d/m/Y H:i') }}
                        </p>
                        <div class="flex items-center justify-between md:justify-end">
                            <a href="{{ route('admin.forms.share', ['id' => $item->id]) }}"
                                class="text-gray-900 bg-white hover:bg-gray-100 border border-gray-200 focus:ring-4 focus:outline-none focus:ring-gray-100 font-medium rounded-lg text-sm px-2 py-1 text-center inline-flex items-center dark:focus:ring-gray-600 dark:bg-gray-800 dark:border-gray-700 dark:text-white dark:hover:bg-gray-700 mr-2 mb-2">
                                <span class="text-blue-500 mr-2">
                                    <x-share />
                                </span>
                                Compartir
                            </a>
                            <a href="{{route('form.show',['form' => $item->id])}}"
                                class="text-gray-900 bg-white hover:bg-gray-100 border border-gray-200 focus:ring-4 focus:outline-none focus:ring-gray-100 font-medium rounded-lg text-sm px-2 py-1 text-center inline-flex items-center dark:focus:ring-gray-600 dark:bg-gray-800 dark:border-gray-700 dark:text-white dark:hover:bg-gray-700 mr-2 mb-2">
                                <span class="text-green-500 mr-2">
                                    <x-eye />
                                </span>
                                Ver Resultados
                            </a>

                        </div>
                    </div>
                @endforeach

            </div>
        @endforeach
    </div>
@endsection
