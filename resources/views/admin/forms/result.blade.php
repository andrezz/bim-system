@extends('layout.admin')
@section('content')
    <div class="container mx-auto px-4">
        <nav class="flex py-4 mb-4" aria-label="Breadcrumb">
            <ol class="inline-flex items-center space-x-1 md:space-x-3">
                <li class="inline-flex items-center">
                    <a href="{{ route('admin.users.dashboard') }}"
                        class="inline-flex items-center text-sm font-medium text-gray-700 hover:text-gray-900 dark:text-gray-400 dark:hover:text-white">
                        <x-home />
                        Inicio
                    </a>
                </li>
                <li class="inline-flex items-center">
                    <a href="{{ route('form.index') }}"
                        class="inline-flex items-center text-sm font-medium text-gray-700 hover:text-gray-900 dark:text-gray-400 dark:hover:text-white">
                        <x-chevron-right />
                        Encuestas
                    </a>
                </li>
                <li aria-current="page">
                    <div class="flex items-center">
                        <x-chevron-right />
                        <span class="ml-1 text-sm font-medium text-gray-500 md:ml-2 dark:text-gray-400">Resultados</span>
                    </div>
                </li>
            </ol>
        </nav>

        <h1 class="font-bold text-3xl">Resultados de la Encuesta</h1>
        <p class="text-medium">{{ $form->description }} - <span class="font-bold">{{ $form->organization->name }}</span></p>
        <hr class="mt-2 mb-6">
        <ul class="list-inside list-disc mb-8">
            @for ($roundNumber = 1; $roundNumber <= $form->rounds; $roundNumber++)
                @php
                    $surveyed = 1;
                @endphp
                <li class="py-1">
                    <a class="text-blue-500 hover:underline font-bold" href="#round_{{ $roundNumber }}">{{ $roundNumber }}
                        Ronda</a>
                    <ul class="list-inside list-disc ml-6">
                        @foreach ($form->applications as $application)
                            @if ($roundNumber == $application->round_number)
                                <li class="py-1">
                                    <a class="text-blue-500 hover:underline" href="#application_{{ $application->id }}">
                                        {{ $surveyed }}.- {{ $application->surveyed->name }}
                                        @php
                                            $surveyed++;
                                        @endphp
                                    </a>
                                </li>
                            @endif
                        @endforeach
                        <li class="py-1">
                            <a class="text-blue-500 hover:underline" href="#summary_{{ $roundNumber }}">Consolidado de
                                Ronda {{ $roundNumber }}</a>
                        </li>
                    </ul>
                </li>
            @endfor
            <li class="py-1">
                <a class="text-blue-500 hover:underline font-bold" href="#summary">Consolidado de la(s)
                    {{ $form->rounds }} Ronda(s)</a>
            </li>
        </ul>


        @for ($roundNumber = 1; $roundNumber <= $form->rounds; $roundNumber++)
            @php
                $surveyed = 1;
                $appCount = 0;
                foreach ($form->applications as $appTmp) {
                    if ($appTmp->round_number == $roundNumber) {
                        $appCount = $appCount + 1;
                    }
                }
            @endphp
            <h1 id="round_{{ $roundNumber }}" class="font-bold text-2xl">{{ $roundNumber }} Ronda</h1>
            <hr class="mt-2 mb-4">
            @foreach ($form->applications as $application)
                @if ($roundNumber == $application->round_number)
                    <div class="mb-8 overflow-x-auto">
                        <table class="w-full">
                            <thead>
                                <tr id="application_{{ $application->id }}" class="bg-blue-700 text-white">
                                    <th class="border border-blue-800 px-3 py-2 uppercase"
                                        colspan="{{ count($levels) + 3 }}">
                                        {{ $surveyed }}.- {{ $application->surveyed->name }}
                                        @php
                                            $surveyed++;
                                        @endphp
                                    </th>
                                </tr>
                                <tr class="bg-blue-700 text-white">
                                    <th rowspan="2" class="border border-blue-800 px-3 py-2">SERIE DE CAPACIDADES</th>
                                    <th rowspan="2" class="border border-blue-800 px-3 py-2">ÁREA DE MADUREZ</th>
                                    <th colspan="{{ count($levels) }}" class="border border-blue-800 px-3 py-2">NIVEL Y
                                        VALOR</th>
                                    <th rowspan="2" class="border border-indigo-300 px-3 py-2">ÍNDICE DE MADUREZ</th>
                                </tr>
                                <tr class="bg-blue-700 text-white">
                                    @foreach ($levels as $level)
                                        <th class="border border-blue-800 px-3 py-2">{{ $level->name }}
                                            ({{ $level->score }})
                                        </th>
                                    @endforeach
                                </tr>
                            </thead>
                            <tbody>
                                @php
                                    $areasCount = 0;
                                @endphp
                                @foreach ($series as $keySerie => $serie)
                                    @foreach ($serie->areas as $keyArea => $area)
                                        @php
                                            $areasCount++;
                                        @endphp
                                        <tr>
                                            @if ($keyArea == 0)
                                                <td rowspan="{{ count($serie->areas) }}"
                                                    class="border border-slate-300 px-3 py-2">{{ $serie->name }}</td>
                                            @endif
                                            <td class="border border-slate-300 px-3 py-2">{{ $area->name }}</td>
                                            @foreach ($levels as $level)
                                                <td class="border border-slate-300 px-3 py-2 text-center">
                                                    @foreach ($application->answers as $answer)
                                                        @if ($answer->area_id == $area->id && $answer->question->level_id == $level->id)
                                                            {{ $answer->score }}
                                                        @endif
                                                    @endforeach
                                                </td>
                                            @endforeach
                                            @if ($keyArea == 0)
                                                <td rowspan="{{ count($serie->areas) }}"
                                                    class="border border-slate-300 px-3 py-2 text-center">
                                                    @php
                                                        $serieAverage = 0;
                                                        foreach ($levels as $level) {
                                                            foreach ($application->answers as $answer) {
                                                                foreach ($serie->areas as $areaTmp) {
                                                                    if ($answer->area_id == $areaTmp->id && $answer->question->level_id == $level->id) {
                                                                        $serieAverage = $serieAverage + $answer->score;
                                                                    }
                                                                }
                                                            }
                                                        }
                                                        $serieAverage = $serieAverage / count($serie->areas);
                                                    @endphp
                                                    {{ round($serieAverage, 2) }}
                                                </td>
                                            @endif
                                        </tr>
                                    @endforeach
                                @endforeach
                                <tr class="font-bold">
                                    <td class="border border-slate-300 px-3 py-2 text-center" colspan="2">VALOR POR NIVEL
                                        DE MADUREZ</td>
                                    @php
                                        $levelsSum = 0;
                                    @endphp
                                    @foreach ($levels as $level)
                                        <td class="border border-slate-300 px-3 py-2 text-center">
                                            @php
                                                $levelSum = 0;
                                                foreach ($application->answers as $answer) {
                                                    if ($answer->question->level_id == $level->id) {
                                                        $levelSum += $answer->score;
                                                    }
                                                }
                                                $levelsSum += $levelSum;
                                            @endphp
                                            {{ $levelSum }}
                                        </td>
                                    @endforeach
                                    <td class="border border-slate-300 px-3 py-2 text-center">{{ $levelsSum }}</td>
                                </tr>
                                <tr class="font-bold text-blue-700">
                                    <td class="border border-slate-300 px-3 py-2 text-center"
                                        colspan="{{ count($levels) + 2 }}">ÍNDICE DE
                                        MADUREZ BIM</td>
                                    <td class="border border-slate-300 px-3 py-2 text-center">
                                        {{ round($levelsSum / $areasCount, 2) }}</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                @endif
            @endforeach

            <div class="mb-8 overflow-x-auto">
                <table class="w-full">
                    <thead>
                        <tr id="summary_{{ $roundNumber }}" class="bg-green-600 text-white">
                            <th class="border border-green-700 px-3 py-2" colspan="{{ count($levels) + 4 }}">
                                CONSOLIDADO DE LA RONDA {{ $roundNumber }}
                            </th>
                        </tr>
                        <tr class="bg-green-600 text-white">
                            <th rowspan="2" class="border border-green-700 px-3 py-2">SERIE DE CAPACIDADES</th>
                            <th rowspan="2" class="border border-green-700 px-3 py-2">ÁREA DE MADUREZ</th>
                            <th colspan="{{ count($levels) }}" class="border border-green-700 px-3 py-2">NIVEL Y VALOR</th>
                            <th rowspan="2" class="border border-green-700 px-3 py-2">ÍNDICE SEGÚN ÁREA DE MADUREZ</th>
                            <th rowspan="2" class="border border-green-700 px-3 py-2">ÍNDICE SEGÚN SERIE DE CAPACIDADES
                            </th>
                        </tr>
                        <tr class="bg-green-600 text-white">
                            @foreach ($levels as $level)
                                <th class="border border-green-700 px-3 py-2">{{ $level->name }}
                                    ({{ $level->score }})
                                </th>
                            @endforeach
                        </tr>
                    </thead>
                    <tbody>
                        @php
                            $seriesSum = 0;
                        @endphp
                        @foreach ($series as $keySerie => $serie)
                            @foreach ($serie->areas as $keyArea => $area)
                                <tr>
                                    @if ($keyArea == 0)
                                        <td rowspan="{{ count($serie->areas) }}" class="border border-slate-300 px-3 py-2">
                                            {{ $serie->name }}</td>
                                    @endif
                                    <td class="border border-slate-300 px-3 py-2">{{ $area->name }}</td>
                                    @foreach ($levels as $level)
                                        <td class="border border-slate-300 px-3 py-2 text-center">
                                            @php
                                                $levelSum = 0;
                                                foreach ($form->applications as $application) {
                                                    if ($roundNumber == $application->round_number) {
                                                        foreach ($application->answers as $answer) {
                                                            if ($answer->area_id == $area->id && $answer->question->level_id == $level->id) {
                                                                $levelSum += $answer->score;
                                                            }
                                                        }
                                                    }
                                                }
                                            @endphp
                                            {{ $levelSum }}
                                        </td>
                                    @endforeach

                                    <td class="border border-slate-300 px-3 py-2 text-center">
                                        @php
                                            $areaAverage = 0;
                                            foreach ($levels as $level) {
                                                foreach ($form->applications as $application) {
                                                    if ($roundNumber == $application->round_number) {
                                                        foreach ($application->answers as $answer) {
                                                            if ($answer->area_id == $area->id && $answer->question->level_id == $level->id) {
                                                                $areaAverage += $answer->score;
                                                            }
                                                        }
                                                    }
                                                }
                                            }

                                            if ($appCount > 0) {
                                                $areaAverage = $areaAverage / $appCount;
                                            } else {
                                                $areaAverage = 0;
                                            }
                                        @endphp
                                        {{ round($areaAverage, 2) }}
                                    </td>
                                    @if ($keyArea == 0)
                                        <td rowspan="{{ count($serie->areas) }}"
                                            class="border border-slate-300 px-3 py-2 text-center">
                                            @php
                                                $serieAverage = 0;
                                                foreach ($levels as $level) {
                                                    foreach ($form->applications as $application) {
                                                        if ($roundNumber == $application->round_number) {
                                                            foreach ($application->answers as $answer) {
                                                                foreach ($serie->areas as $areaTmp) {
                                                                    if ($answer->area_id == $areaTmp->id && $answer->question->level_id == $level->id) {
                                                                        $serieAverage = $serieAverage + $answer->score;
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                                if ($appCount > 0) {
                                                    $serieAverage = $serieAverage / (count($serie->areas) * $appCount);
                                                } else {
                                                    $serieAverage = 0;
                                                }
                                                $seriesSum += $serieAverage;
                                            @endphp
                                            {{ round($serieAverage, 2) }}
                                        </td>
                                    @endif
                                </tr>
                            @endforeach
                        @endforeach

                        <tr class="font-bold">
                            <td class="border border-slate-300 px-3 py-2 text-center" colspan="2">VALOR POR NIVEL
                                DE MADUREZ</td>
                            @php
                                $levelsSum = 0;
                            @endphp
                            @foreach ($levels as $level)
                                <td class="border border-slate-300 px-3 py-2 text-center">
                                    @php
                                        $levelSum = 0;
                                        foreach ($form->applications as $application) {
                                            if ($roundNumber == $application->round_number) {
                                                foreach ($application->answers as $answer) {
                                                    if ($answer->question->level_id == $level->id) {
                                                        $levelSum += $answer->score;
                                                    }
                                                }
                                            }
                                        }
                                        $levelsSum += $levelSum;
                                    @endphp
                                    {{ $levelSum }}
                                </td>
                            @endforeach
                            <td class="border border-slate-300 px-3 py-2 text-center" colspan="2">{{ $levelsSum }}
                            </td>
                        </tr>
                        <tr class="font-bold text-green-500">
                            <td class="border border-slate-300 px-3 py-2 text-center" colspan="{{ count($levels) + 3 }}">
                                ÍNDICE DE
                                MADUREZ BIM</td>
                            <td class="border border-slate-300 px-3 py-2 text-center">
                                {{ round($seriesSum / count($series), 2) }}</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        @endfor


        <div class="mb-8 overflow-x-auto">
            <table class="w-full">
                <thead>
                    <tr id="summary" class="bg-indigo-500 text-white">
                        <th class="border border-indigo-600 px-3 py-2" colspan="{{ count($levels) + 4 }}">
                            CONSOLIDADO DE LAS {{ $form->rounds }} RONDAS
                        </th>
                    </tr>
                    <tr class="bg-indigo-500 text-white">
                        <th rowspan="2" class="border border-indigo-600 px-3 py-2">SERIE DE CAPACIDADES</th>
                        <th rowspan="2" class="border border-indigo-600 px-3 py-2">ÁREA DE MADUREZ</th>
                        <th colspan="{{ count($levels) }}" class="border border-indigo-600 px-3 py-2">NIVEL Y VALOR</th>
                        <th rowspan="2" class="border border-indigo-600 px-3 py-2">ÍNDICE SEGÚN ÁREA DE MADUREZ</th>
                        <th rowspan="2" class="border border-indigo-600 px-3 py-2">ÍNDICE SEGÚN SERIE DE CAPACIDADES
                        </th>
                    </tr>
                    <tr class="bg-indigo-500 text-white">
                        @foreach ($levels as $level)
                            <th class="border border-indigo-600 px-3 py-2">{{ $level->name }}
                                ({{ $level->score }})
                            </th>
                        @endforeach
                    </tr>
                </thead>
                <tbody>
                    @php
                        $maxApp = 0;
                        for ($rnd = 1; $rnd <= $form->rounds; $rnd++) {
                            $rndCount = 0;
                            foreach ($form->applications as $appTmp) {
                                if ($appTmp->round_number == $rnd) {
                                    $rndCount++;
                                }
                            }
                            if ($rndCount > $maxApp) {
                                $maxApp = $rndCount;
                            }
                        }
                        $seriesSum = 0;
                    @endphp
                    @foreach ($series as $keySerie => $serie)
                        @foreach ($serie->areas as $keyArea => $area)
                            <tr>
                                @if ($keyArea == 0)
                                    <td rowspan="{{ count($serie->areas) }}" class="border border-slate-300 px-3 py-2">
                                        {{ $serie->name }}</td>
                                @endif
                                <td class="border border-slate-300 px-3 py-2">{{ $area->name }}</td>
                                @foreach ($levels as $level)
                                    <td class="border border-slate-300 px-3 py-2 text-center">
                                        @php
                                            $levelSum = 0;
                                            foreach ($form->applications as $application) {
                                                foreach ($application->answers as $answer) {
                                                    if ($answer->area_id == $area->id && $answer->question->level_id == $level->id) {
                                                        $levelSum += $answer->score;
                                                    }
                                                }
                                            }
                                        @endphp
                                        {{ $levelSum }}
                                    </td>
                                @endforeach

                                <td class="border border-slate-300 px-3 py-2 text-center">
                                    @php
                                        $areaAverage = 0;
                                        foreach ($levels as $level) {
                                            foreach ($form->applications as $application) {
                                                foreach ($application->answers as $answer) {
                                                    if ($answer->area_id == $area->id && $answer->question->level_id == $level->id) {
                                                        $areaAverage = $areaAverage + $answer->score;
                                                    }
                                                }
                                            }
                                        }

                                        if ($maxApp > 0) {
                                            $areaAverage = $areaAverage / $maxApp / $form->rounds;
                                        } else {
                                            $areaAverage = 0;
                                        }
                                    @endphp
                                    {{ round($areaAverage, 2) }}
                                </td>
                                @if ($keyArea == 0)
                                    <td rowspan="{{ count($serie->areas) }}"
                                        class="border border-slate-300 px-3 py-2 text-center">
                                        @php
                                            $serieAverage = 0;
                                            foreach ($levels as $level) {
                                                foreach ($form->applications as $application) {
                                                    foreach ($application->answers as $answer) {
                                                        foreach ($serie->areas as $areaTmp) {
                                                            if ($answer->area_id == $areaTmp->id && $answer->question->level_id == $level->id) {
                                                                $serieAverage += $answer->score;
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                            if ($maxApp > 0) {
                                                $serieAverage = $serieAverage / $maxApp / count($serie->areas) / $form->rounds;
                                            } else {
                                                $serieAverage = 0;
                                            }
                                            $seriesSum += $serieAverage;
                                        @endphp
                                        {{ round($serieAverage, 2) }}
                                    </td>
                                @endif
                            </tr>
                        @endforeach
                    @endforeach

                    <tr class="font-bold">
                        <td class="border border-slate-300 px-3 py-2 text-center" colspan="2">VALOR POR NIVEL
                            DE MADUREZ</td>
                        @php
                            $levelsSum = 0;
                        @endphp
                        @foreach ($levels as $level)
                            <td class="border border-slate-300 px-3 py-2 text-center">
                                @php
                                    $levelSum = 0;
                                    foreach ($form->applications as $application) {
                                        foreach ($application->answers as $answer) {
                                            if ($answer->question->level_id == $level->id) {
                                                $levelSum += $answer->score;
                                            }
                                        }
                                    }
                                    $levelsSum += $levelSum;
                                @endphp
                                {{ $levelSum }}
                            </td>
                        @endforeach
                        <td class="border border-slate-300 px-3 py-2 text-center" colspan="2">{{ $levelsSum }}</td>
                    </tr>
                    <tr class="font-bold text-indigo-500">
                        <td class="border border-slate-300 px-3 py-2 text-center" colspan="{{ count($levels) + 3 }}">
                            ÍNDICE DE
                            MADUREZ BIM</td>
                        <td class="border border-slate-300 px-3 py-2 text-center">
                            {{round($seriesSum / count($series),2)}}
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
@endsection
