@extends('layout.admin')
@section('content')
    <div class="container mx-auto px-4">
        <div class="flex items-center justify-between mb-4">
            <nav class="flex py-4 flex-1" aria-label="Breadcrumb">
                <ol class="inline-flex items-center space-x-1 md:space-x-3">
                    <li class="inline-flex items-center">
                        <a href="{{ route('admin.users.dashboard') }}"
                            class="inline-flex items-center text-sm font-medium text-gray-700 hover:text-gray-900 dark:text-gray-400 dark:hover:text-white">
                            <x-home />
                            Inicio
                        </a>
                    </li>
                    <li aria-current="page">
                        <div class="flex items-center">
                            <x-chevron-right />
                            <span
                                class="ml-1 text-sm font-medium text-gray-500 md:ml-2 dark:text-gray-400">Organizaciones</span>
                        </div>
                    </li>
                </ol>
            </nav>

            <a href="{{ route('organization.create') }}"
                class="text-white bg-indigo-700 hover:bg-indigo-800 focus:ring-4 focus:outline-none focus:ring-indigo-300 font-medium rounded-lg text-sm px-5 py-2.5 text-center dark:bg-indigo-600 dark:hover:bg-indigo-700 dark:focus:ring-indigo-800">
                Nueva Organización
            </a>
        </div>

        @foreach ($organizations as $item)
            <div
                class="border border-slate-300 dark:border-slate-800 dark:bg-slate-800 rounded p-3 mb-4 flex items-center justify-end hover:bg-indigo-100 dark:hover:bg-slate-700">
                <div class="flex-1">
                    <h2 class="font-bold text-medium dark:text-white">{{ $item->name }}</h2>
                    <p class="text-slate-500 text-sm">{{ $item->code }}</p>
                    <span
                        class="text-{{ $item->type == 'Privada' ? 'green' : 'blue' }}-500 font-bold text-sm">{{ $item->type }}</span>
                </div>
                <div class="flex ml-auto">
                    <a href="{{ route('organization.edit', ['organization' => $item->id]) }}"
                        class="text-gray-800 bg-white hover:bg-gray-100 border border-gray-200 focus:ring-4 focus:outline-none focus:ring-gray-100 font-medium rounded-lg text-sm px-4 py-2 text-center inline-flex items-center dark:focus:ring-gray-600 dark:bg-gray-800 dark:border-gray-700 dark:text-white dark:hover:bg-gray-700 mr-2 mb-2">
                        <x-pencil-square />
                    </a>
                    <form id="form_delete_{{ $item->id }}"
                        action="{{ route('organization.destroy', ['organization' => $item->id]) }}" method="POST">
                        @csrf
                        @method('DELETE')
                    </form>

                    <button onclick="confirmDelete('form_delete_{{ $item->id }}')"
                        class="text-pink-500 bg-white hover:bg-gray-100 border border-gray-200 focus:ring-4 focus:outline-none focus:ring-gray-100 font-medium rounded-lg text-sm px-4 py-2 text-center inline-flex items-center dark:focus:ring-gray-600 dark:bg-gray-800 dark:border-gray-700 dark:text-white dark:hover:bg-gray-700 mr-2 mb-2">
                        <x-trash />
                    </button>
                </div>
            </div>
        @endforeach

    </div>
@endsection
