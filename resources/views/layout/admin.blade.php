<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Panel del Administrador</title>

    <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
    <link rel="manifest" href="/site.webmanifest">
    <link rel="mask-icon" href="/safari-pinned-tab.svg" color="#5bbad5">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="theme-color" content="#ffffff">

    <link href="{{ mix('css/app.css') }}" rel="stylesheet">
    <script src="{{ mix('js/app.js') }}"></script>
</head>

<body class="dark:bg-slate-900">
    <div class="flex items-center justify-start border-slate-300 dark:border-slate-900 dark:bg-slate-800 border-b py-3 px-4 mb-4">
        <img src="{{asset('images/logo.svg')}}" alt="logo">
        <h1 class="text-lg font-bold mx-2 dark:text-white">Panel de Control</h1>

        <a class="text-indigo-500 text-sm ml-auto font-bold dark:text-indigo-400" href="{{url('/')}}">Salir</a>
    </div>

    @yield('content')
</body>

</html>
