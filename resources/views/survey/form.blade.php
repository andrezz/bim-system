@extends('layout.survey')
@section('content')
    <x-appbar :title=" $application->round_number . ' Ronda' " />
    <div class="container mx-auto p-4">
        <div class="w-1/3 bg-gray-200 rounded-full h-2.5 dark:bg-gray-700 mb-6">
            <div class="bg-indigo-600 h-2.5 rounded-full" style="width: {{ $progress }}%"></div>
        </div>
        <h1 class="text-md font-bold dark:text-white">Serie de Capacidades: <span class="text-orange-500">{{ $area->serie->name }}</span></h1>
        <h2 class="text-xl font-bold dark:text-white">Area de Madurez: <span class="text-indigo-500">{{ $area->name }}</span></h2>
        <p class="mb-6 text-slate-500">{{ $area->description }}</p>

        <form action="{{ route('answer.store') }}" method="POST">
            @csrf
            <input type="hidden" name="area_id" value="{{$area->id}}">
            <input type="hidden" name="application_id" value="{{$application->id}}">
            <input type="hidden" name="code" value="{{$code}}">
            <div class="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-3 gap-4">
                @foreach ($questions as $item)
                <x-question :id="'question_'. $item->id" :value="$item->id" :title="$item->level->name" name="question_id" :description="$item->question" />
            @endforeach
            </div>

            <div class="flex items-center justify-between mt-4">
                <a href="{{ url('/') }}"
                    class="text-center py-2.5 px-5 mr-2 mb-2 text-sm font-medium text-gray-900 focus:outline-none bg-white rounded-lg border border-gray-200 hover:bg-gray-100 hover:text-indigo-700 focus:z-10 focus:ring-4 focus:ring-gray-200 dark:focus:ring-gray-700 dark:bg-gray-800 dark:text-gray-400 dark:border-gray-600 dark:hover:text-white dark:hover:bg-gray-700">Cancelar</a>
                <button type="submit"
                    class="text-center text-white bg-indigo-700 hover:bg-indigo-800 focus:ring-4 focus:ring-indigo-300 font-medium rounded-lg text-sm px-5 py-2.5 mr-2 mb-2 dark:bg-indigo-600 dark:hover:bg-indigo-700 focus:outline-none dark:focus:ring-indigo-800">Continuar</button>
            </div>
        </form>
    </div>
@endsection
