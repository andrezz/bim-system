<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Resultado de la Encuesta BIM</title>
    <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
    <link rel="manifest" href="/site.webmanifest">
    <link rel="mask-icon" href="/safari-pinned-tab.svg" color="#5bbad5">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="theme-color" content="#ffffff">
    <style>
        .border {
            border-width: 1px;
            border-style: solid;
        }

        .border-blue-800 {
            border-color: rgb(30,64,175);
        }

        .bg-blue-700 {
            background-color: rgb(29,78,216);
        }

        .text-white {
            color: #fff
        }

        .uppercase {
            text-transform: uppercase;
        }

        .px-3 {
            padding-left: 0.25rem;
            padding-right: 0.25rem;
        }

        .py-2 {
            padding-top: 0.25rem;
            padding-bottom: 0.25rem;
        }

        .border-slate-300 {
            border-color: rgb(203,213,225);
        }

        .text-center {
            text-align: center;
        }

        table {
            border-collapse: collapse;
        }
        th, td {
            font-size: 13px;
        }
        .w-full {
            width: 19cm;
        }
    </style>
</head>

<body>
    <table class="w-full">
        <thead>
            <tr id="application_{{ $application->id }}" class="bg-blue-700 text-white">
                <th class="border border-blue-800 px-3 py-2 uppercase" colspan="{{ count($levels) + 3 }}">
                    {{ $application->surveyed->name }}
                </th>
            </tr>
            <tr class="bg-blue-700 text-white">
                <th rowspan="2" class="border border-blue-800 px-3 py-2">SERIE DE CAPACIDADES</th>
                <th rowspan="2" class="border border-blue-800 px-3 py-2">ÁREA DE MADUREZ</th>
                <th colspan="{{ count($levels) }}" class="border border-blue-800 px-3 py-2">NIVEL Y
                    VALOR</th>
                <th rowspan="2" class="border border-indigo-300 px-3 py-2">ÍNDICE DE MADUREZ</th>
            </tr>
            <tr class="bg-blue-700 text-white">
                @foreach ($levels as $level)
                    <th class="border border-blue-800 px-3 py-2">{{ $level->name }}
                        ({{ $level->score }})
                    </th>
                @endforeach
            </tr>
        </thead>
        <tbody>
            @php
                $areasCount = 0;
            @endphp
            @foreach ($series as $keySerie => $serie)
                @foreach ($serie->areas as $keyArea => $area)
                    @php
                        $areasCount++;
                    @endphp
                    <tr>
                        @if ($keyArea == 0)
                            <td rowspan="{{ count($serie->areas) }}" class="border border-slate-300 px-3 py-2">
                                {{ $serie->name }}</td>
                        @endif
                        <td class="border border-slate-300 px-3 py-2">{{ $area->name }}</td>
                        @foreach ($levels as $level)
                            <td class="border border-slate-300 px-3 py-2 text-center">
                                @foreach ($application->answers as $answer)
                                    @if ($answer->area_id == $area->id && $answer->question->level_id == $level->id)
                                        {{ $answer->score }}
                                    @endif
                                @endforeach
                            </td>
                        @endforeach
                        @if ($keyArea == 0)
                            <td rowspan="{{ count($serie->areas) }}"
                                class="border border-slate-300 px-3 py-2 text-center">
                                @php
                                    $serieAverage = 0;
                                    foreach ($levels as $level) {
                                        foreach ($application->answers as $answer) {
                                            foreach ($serie->areas as $areaTmp) {
                                                if ($answer->area_id == $areaTmp->id && $answer->question->level_id == $level->id) {
                                                    $serieAverage = $serieAverage + $answer->score;
                                                }
                                            }
                                        }
                                    }
                                    $serieAverage = $serieAverage / count($serie->areas);
                                @endphp
                                {{ round($serieAverage, 2) }}
                            </td>
                        @endif
                    </tr>
                @endforeach
            @endforeach
            <tr class="font-bold">
                <td class="border border-slate-300 px-3 py-2 text-center" colspan="2">VALOR POR NIVEL
                    DE MADUREZ</td>
                @php
                    $levelsSum = 0;
                @endphp
                @foreach ($levels as $level)
                    <td class="border border-slate-300 px-3 py-2 text-center">
                        @php
                            $levelSum = 0;
                            foreach ($application->answers as $answer) {
                                if ($answer->question->level_id == $level->id) {
                                    $levelSum += $answer->score;
                                }
                            }
                            $levelsSum += $levelSum;
                        @endphp
                        {{ $levelSum }}
                    </td>
                @endforeach
                <td class="border border-slate-300 px-3 py-2 text-center">{{ $levelsSum }}</td>
            </tr>
            <tr class="font-bold text-blue-700">
                <td class="border border-slate-300 px-3 py-2 text-center" colspan="{{ count($levels) + 2 }}">ÍNDICE DE
                    MADUREZ BIM</td>
                <td class="border border-slate-300 px-3 py-2 text-center">
                    {{ round($levelsSum / $areasCount, 2) }}</td>
            </tr>
        </tbody>
    </table>
</body>

</html>
