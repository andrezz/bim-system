@extends('layout.survey')
@section('content')
    <div class="container p-4 mx-auto">

        <div class="flex items-center justify-center mt-6 mb-6">
            <img src="{{asset('images/checked.svg')}}" alt="success" class="w-32 h-32">
        </div>

        <p class="text-center mb-4 text-lg font-normal text-gray-500 lg:text-xl dark:text-gray-400">
            Encuesta finalizada. <br>
            El nivel de adopción BIM es:
        </p>
        <h1 class="text-center mb-4 text-5xl font-extrabold tracking-tight leading-none text-indigo-500 dark:text-indigo-300">
            {{$label}}
        </h1>
        <p class="text-center mb-8 text-lg font-normal text-blue-500 lg:text-xl dark:text-gray-400 mb-6">
            Indice de Madurez <br>
            <span class="text-3xl font-bold">{{round($value, 2)}}</span>
        </p>

        <div class="flex items-center justify-center">
            <a href="{{route('survey.welcome')}}" class="text-center py-2.5 px-5 mr-2 mb-2 text-sm font-medium text-gray-900 focus:outline-none bg-white rounded-lg border border-gray-200 hover:bg-gray-100 hover:text-indigo-700 focus:z-10 focus:ring-4 focus:ring-gray-200 dark:focus:ring-gray-700 dark:bg-gray-800 dark:text-gray-400 dark:border-gray-600 dark:hover:text-white dark:hover:bg-gray-700">Ir al Inicio</a>
            <a target="_blank" href="{{route('survey.resultpdf',['application' => $id])}}" class="text-center text-white bg-indigo-700 hover:bg-indigo-800 focus:ring-4 focus:ring-indigo-300 font-medium rounded-lg text-sm px-5 py-2.5 mr-2 mb-2 dark:bg-indigo-600 dark:hover:bg-indigo-700 focus:outline-none dark:focus:ring-indigo-800">Descargar PDF</a>
        </div>

    </div>
@endsection
