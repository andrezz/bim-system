@extends('layout.survey')
@section('content')
    <div class="grid grid-cols-1 md:grid-cols-2 gap-4">
        <div class="p-8 text-center md:text-left">
            <div class="flex justify-center md:justify-start mb-3">
                <img src="{{ asset('images/logo.svg') }}" alt="logo" class="w-16 h-16">
            </div>
            <h1
                class="mb-4 text-4xl font-extrabold tracking-tight leading-none text-gray-900 md:text-5xl lg:text-6xl dark:text-white">
                Software para medir el nivel de adopción BIM</h1>
            <p class="mb-6 text-lg font-normal text-gray-500 lg:text-xl dark:text-gray-400">
                Para Organizaciones Públicas y Privadas
            </p>
            <div class="grid grid-cols-1 lg:grid-cols-5 gap-4">
                <a href="{{ route('admin.users.login') }}"
                    class="col-span-2 text-white bg-gradient-to-br from-purple-600 to-blue-500 hover:bg-gradient-to-bl focus:ring-4 focus:outline-none focus:ring-blue-300 dark:focus:ring-blue-800 font-medium rounded-lg text-sm px-5 py-3 text-center mr-2">
                    Configurar Encuesta
                </a>
                <form autocomplete="off" class="flex items-center justify-between lg:col-span-3" action="{{route('forms.applyCode')}}" method="GET">
                    <input type="text" name="code" id="code"
                        class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-primary-600 focus:border-primary-600 block w-full px-3 py-3 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-indigo-500 dark:focus:border-indigo-500"
                        placeholder="Ingresar Código" required="">
                    <button
                        class="text-indigo-500 px-5 py-2 font-bold disabled:text-slate-400 whitespace-nowrap text-sm">
                        Abrir Encuesta
                    </button>
                </form>
            </div>
        </div>
        <div class="p-4">
            <img src="{{ asset('images/undraw_building_re_xfcm.svg') }}" class="w-full h-80 object-contain" alt="welcome">
        </div>
    </div>
@endsection
