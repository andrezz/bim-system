@extends('layout.survey')
@section('content')
    <x-appbar title="Datos del Encuestado" />
    <div class="container p-4 mx-auto">
        <form action="{{ route('surveyed.store') }}" method="POST">
            @csrf
            <input type="hidden" name="organization_id" value="{{ $organization->id }}">
            <input type="hidden" name="form_code" value="{{ $code }}">
            <div class="grid grid-cols-1 md:grid-cols-2 gap-4">
                <div class="mb-3">
                    <label for="name" class="block mb-2 text-sm font-medium text-gray-900 dark:text-gray-300">Nombres y
                        Apellidos</label>
                    <input type="text" id="name" name="name" maxlength="255"
                        class=" bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-indigo-500 focus:border-indigo-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-indigo-500 dark:focus:border-indigo-500"
                        placeholder="Juan Perez" required>
                </div>
                <div class="mb-3">
                    <label for="code"
                        class="block mb-2 text-sm font-medium text-gray-900 dark:text-gray-300">DNI</label>
                    <input type="text" id="code" name="code" maxlength="8"
                        class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-indigo-500 focus:border-indigo-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-indigo-500 dark:focus:border-indigo-500"
                        placeholder="12345678" required>
                </div>
            </div>
            <div class="flex items-center justify-end">
                <button
                    class="text-white bg-indigo-700 hover:bg-indigo-800 focus:ring-4 focus:outline-none focus:ring-indigo-300 font-medium rounded-lg text-sm w-full sm:w-auto px-5 py-2.5 text-center dark:bg-indigo-600 dark:hover:bg-indigo-700 dark:focus:ring-indigo-800">Continuar</button>
            </div>
        </form>
    </div>
@endsection
